ManageIQ Dockerfiles build wrapper
==================================

!["Prompt"](https://raw.githubusercontent.com/gbraad/assets/gh-pages/icons/prompt-icon-64.png)


Build wrapper for [ManageIQ](https://github.com/ManageIQ/manageiq) images.


Usage
-----

### Cloned

  * ManageIQ latest  
    `docker pull registry.gitlab.com/gbraad/manageiq:latest`
  * ManageIQ latest Darga  
    `docker pull registry.gitlab.com/gbraad/manageiq:latest-darga`


### Custom

  * ManageIQ latest (source)  
    `docker pull registry.gitlab.com/gbraad/manageiq:source`
  * ...


Authors
-------

| [!["Gerard Braad"](http://gravatar.com/avatar/e466994eea3c2a1672564e45aca844d0.png?s=60)](http://gbraad.nl "Gerard Braad <me@gbraad.nl>") |
|---|
| [@gbraad](https://twitter.com/gbraad)  |
